import 'package:flutter/material.dart';

class CustomTextButton extends StatelessWidget {
  final Color backgroundColor;
  final String text;

  const CustomTextButton(
      {super.key, required this.backgroundColor, required this.text});

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {},
      style: TextButton.styleFrom(
          backgroundColor: backgroundColor, fixedSize: Size(400, 20)),
      child: Text(
        text,
        style: TextStyle(color: Colors.black, fontSize: 14),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'costam_text.dart';
import 'package:flutter/cupertino.dart';

class costumTextFromField extends StatelessWidget {
  costumTextFromField(
      {super.key,
      required this.text,
      required this.hintText,
      required this.onSave,
      required this.validator});
  final String text;
  final String hintText;
  final Function onSave;
  final Function validator;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          CustomText(text: text, Fontsize: 14, color: Colors.grey),
          TextFormField(
            onSaved: (Value) {
              onSave();
            },
            validator: (value) {
              validator();
            },
            decoration: InputDecoration(
                hintText: hintText, hintStyle: TextStyle(color: Colors.grey)),
          )
        ],
      ),
    );
  }
}

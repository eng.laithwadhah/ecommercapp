import 'package:flutter/material.dart';

class CustomText extends StatelessWidget {
  const CustomText({
    super.key,
    this.alignment = Alignment.topLeft,
    required this.text,
    required this.Fontsize,
    required this.color,
  });
  final Alignment alignment;
  final String text;
  final double Fontsize;
  final Color color;
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: alignment,
      child: Text(
        text,
        style: TextStyle(fontSize: Fontsize, color: color),
      ),
    );
  }
}

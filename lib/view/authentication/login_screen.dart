import 'package:flutter/material.dart';
import 'package:flutter_application_1/constanc.dart';
import 'package:flutter_application_1/view/widget/costam_text.dart';
import 'package:flutter_application_1/view/widget/costum_Text_form_field.dart';
import 'package:flutter_application_1/view/widget/custom_text_button.dart';

class loginScreen extends StatefulWidget {
  const loginScreen({super.key});

  @override
  State<loginScreen> createState() => _loginState();
}

class _loginState extends State<loginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.white,
        ),
        body: Padding(
          padding: EdgeInsets.only(top: 50, right: 20, left: 20),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CustomText(text: 'Welcom', Fontsize: 25, color: Colors.black),
                  CustomText(text: 'sign up', Fontsize: 18, color: primaryColor)
                ],
              ),
              SizedBox(height: 30),
              CustomText(
                  text: 'Sign in to countinue',
                  Fontsize: 14,
                  color: Colors.grey),
              SizedBox(
                height: 30,
              ),
              costumTextFromField(
                  text: 'Email',
                  hintText: 'put your Email@gmail.com',
                  onSave: (Value) {},
                  validator: (Value) {}),
              SizedBox(
                height: 20,
              ),
              costumTextFromField(
                  text: 'Passward',
                  hintText: '*********',
                  onSave: (Value) {},
                  validator: (Value) {}),
              SizedBox(
                height: 15,
              ),
              CustomText(
                text: 'forget passward',
                Fontsize: 15,
                color: Colors.black,
                alignment: Alignment.topRight,
              ),
              const SizedBox(
                height: 15,
              ),
              CustomTextButton(backgroundColor: primaryColor, text: 'login'),
              SizedBox(height: 20,),
              CustomTextButton(backgroundColor: Colors.blue, text: 'Facebook')
            ],
          ),
        ));
  }
}
